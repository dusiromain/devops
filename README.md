# DevOps

Groupe :
- Clément COSTA
- Aymerick Bres
- Martin BONELLI
- Romain DUSI
- Sofiene Rassas


Classe :
- B3C1 


Comment utiliser l'image Docker :
- Télécharger la dernière version de l'image avec la commande : docker pull marethyu34/devops:latest
- Lancer le container avec la commande :
docker run -d -p 8080:8080 marethyu34/devops:latest
- Allez sur le site avec l'url :
IP:8080


Pour obtenir l'IP de la machine hôte vous pouvez faire :
- Windows : ipconfig
- Linux : "ifconfig" ou "ip a"