<!DOCTYPE html>

<html>
  <head>
    <title>Formation approche DevOps</title>
  </head>
  
  <body>
      <h1>Formation approche DevOps</h1></br>
      <h2>Recette de la tarte aux pommes :</h2>
      </br>
      <h3>Ingr�dients :</h3>
      <ul>
          <li>1 p�te bris�e</li>
          <li>6 pommes</li>
          <li>1 sachet de sucre vanill�</li>
          <li>30g de beurre</li>
          <li>200g de compote</li>
      </ul>
      </br>
      <h3>Pr�paration :</h3>
      <ol>
          <li>Eplucher et d�couper les pommes.</li>
          <li>Pr�chauffer le four � 210 C (thermost 7).</li>
          <li>Mettez la p�te dans le moule de votre choix.</li>
          <li>Ajouter la compote et les pommes sur la p�te.</li>
          <li>Versez le sucre sur les pommes et placer des petits morceaux de beurre sur ces derni�res.</li>
          <li>Mettre au four et laisser cuire pendant 30 min max.</li>
      </ol>
  </body>
</html>
