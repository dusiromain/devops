<!DOCTYPE html>

<html>
  <head>
    <title>Formation approche DevOps</title>
  </head>
  
  <body>
      <h1>Formation approche DevOps</h1></br>
      <h2>Recette du gateau � la citrouille :</h2>
      </br>
      <h3>Ingr�dients :</h3>
      <ul>
          <li>800g de citrouille/potiron</li>
          <li>3 oeufs</li>
          <li>4 cuill�res � soupe de farine</li>
          <li>8 cuill�re � soupe de sucre</li>
          <li>1 cuill�re � soupe d'eau de fleur d'oranger</li>
      </ul>
      </br>
      <h3>Pr�paration :</h3>
      <ol>
          <li>Faire cuire la citrouille/potion, �gouter et laisser refroidir.</li>
          <li>Pr�chauffer le four � 230 C.</li>
          <li>M�langer la citrouille/potiron avec les autres ingr�dients.</li>
          <li>Verser dans un moule beurr�.</li>
          <li>Mettre au four � 230 C pendant 5min puis baisser � 190 C pendant 45 min.</li>
      </ol>
  </body>
</html>
