FROM tomcat:8.0

RUN rm -r /usr/local/tomcat/webapps/ROOT
COPY $CI_PROJECT_DIR/target/ROOT.war /usr/local/tomcat/webapps/